//
//  PersonTest.swift
//  WesterosTests
//
//  Created by Rita Casiello on 02/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import XCTest
@testable import Westeros

class PersonTest: XCTestCase {
    
    var starkSigil: Sigil!
    var lannisterSigil: Sigil!
    
    var starkURL: URL!
    var lannisterURL: URL!
    
    var starkHouse: House!
    var lannisterHouse: House!
    
    var ned: Person!
    var robb: Person!
    var tyrion: Person!
    var arya: Person!
    

    override func setUp() {
        
        starkSigil = Sigil(description: "Lobo Huargo", image: UIImage())
        lannisterSigil = Sigil(description: "León Rampante", image: UIImage())
        
        starkURL = URL(string: "https://awoiaf.westeros.org/index.php/House_Stark")!
        lannisterURL = URL(string: "https://awoiaf.westeros.org/index.php/House_Lannister")!
        
        starkHouse = House(name: "Stark", sigil:starkSigil, words:"Winter is coming", url: starkURL)
        lannisterHouse = House(name: "Lannister", sigil:lannisterSigil, words:"Hear my roar", url: lannisterURL)
        
        ned = Person(name: "Eddard", photo: UIImage(named: "robb")!, alias: "Ned", house: starkHouse)
        robb = Person (name: "Robb", photo: UIImage(named: "robb")!, alias: "joven Lobo", house: starkHouse)
        arya = Person (name: "Arya", house: starkHouse, photo: UIImage(named: "arya")!)
        tyrion = Person(name: "Tyrion", photo: UIImage(named: "tyrion")!, alias: "El enano", house: lannisterHouse)
  
    }
    

    override func tearDown() {
    
    }

    func testPersonExistence(){
        XCTAssertNotNil(ned)
    
    }
    
    func testPersonExistenceWithoutAlias(){
        
        arya = Person(name: "Arya", house: starkHouse, photo: UIImage(named: "arya")!)
        XCTAssertNotNil(arya)
        XCTAssertEqual(arya.alias, "")

    }
    
    func testPersonFullName(){
         XCTAssertEqual(ned.fullname, "Eddard Stark")
    }
    
    func testPersonConformsToHashable(){
        XCTAssertNotNil(tyrion.hashValue)
        print (" El hash de este miembro es \(tyrion.hashValue)")
    }
    
    func testPersonConformsToEquality(){
        let eddard = Person(name: "Eddard", photo: UIImage(named: "robb")!, alias: "Ned", house: starkHouse)
        XCTAssertEqual(ned, eddard)
       
        XCTAssertEqual(ned, ned)
        
        XCTAssertNotEqual(ned, tyrion)
        
    }
}

