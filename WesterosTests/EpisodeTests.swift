//
//  EpisodeTests.swift
//  WesterosTests
//
//  Created by Rita Casiello on 11/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import XCTest
@testable import Westeros

class EpisodeTests: XCTestCase {
    
    var season1: Season!
    var season2: Season!
    var ep1s1: Episode!
    var ep2s1: Episode!
    var ep1s2: Episode!
    var ep2s2: Episode!


    override func setUp() {
      
        //temporadas
        season1 = Season(
            name: "Season 1",
            launchDate: Date.parse("17/04/2011"),
            photoSeason: UIImage(named: "season1")!
        )
        season2 = Season(
            name: "Season 2",
            launchDate: Date.parse("01/04/2012"),
            photoSeason: UIImage (named: "season2")!
        )
        //episodios
        ep1s1 = Episode(
            title: "Winter is coming",
            dateIssue: Date.parse("17/04/2011"),
            description: "'Winter Is Coming' is the series premiere episode of the HBO medieval fantasy television series Game of Thrones. The first episode of the first season, it was written by the show creators David Benioff and D. B. Weiss, in a faithful adaptation of the first chapters of George R. R. Martin's book A Game of Thrones. The episode was directed by Tim Van Patten, redoing the work done by director Tom McCarthy in an unaired pilot.",
            season: season1
        )
        ep2s1 = Episode(
            title: "The Kingsroar",
            dateIssue: Date.parse("24/04/2011"),
            description: "'The Kingsroad' is the second episode of the first season of the HBO medieval fantasy television series Game of Thrones, first aired on April 24, 2011. It was written by the show creators David Benioff and D. B. Weiss, and directed by Tim Van Patten.",
            season: season1
        )
        ep1s2 = Episode(
            title: "The North remembers",
            dateIssue: Date.parse("01/04/2012"),
            description: "'The North Remembers' is the second season premiere episode of HBO's fantasy television series Game of Thrones. First aired on April 1, 2012, it was written by the show creators and executive producers David Benioff and D. B. Weiss, and directed by returning director Alan Taylor.",
            season: season2
        )
        ep2s2 = Episode(
            title: "The Night Lands",
            dateIssue: Date.parse("08/04/2012"),
            description: "'The Night Lands' is the second episode of the second season of HBO's medieval fantasy television series Game of Thrones. The episode is written by the showrunners David Benioff and D. B. Weiss, and directed by Alan Taylor. It was first released on April 2, 2012 via the online service HBO GO in some European countries, including the Netherlands, Poland and Slovenia. Cable television first broadcast it on April 8, 2012.",
            season: season2        )
    }

    override func tearDown() {
       
    }
    
    func testEpisodeExistence(){
        XCTAssertNotNil(ep1s2)
        
    }
    
    func testEpisodeEquality(){
        //identidad
        XCTAssertEqual(ep2s2, ep2s2)
        
        //igualdad
        let episode2 = Episode(
            title: "The Kingsroar",
            dateIssue: Date.parse("24/04/2011"),
            description: "'The Kingsroad' is the second episode of the first season of the HBO medieval fantasy television series Game of Thrones, first aired on April 24, 2011. It was written by the show creators David Benioff and D. B. Weiss, and directed by Tim Van Patten.",
            season: season1
        )
        XCTAssertEqual(episode2, ep2s1)
        
        //desigualdad
        XCTAssertNotEqual(episode2, ep2s2)
        
    }
    
    func testEspisodeHashable(){
        XCTAssertNotNil(ep2s1.hashValue)
        print ("El hash del episodio es \(ep2s1.hashValue)")
        
    }

}






