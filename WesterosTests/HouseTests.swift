//
//  HouseTests.swift
//  WesterosTests
//
//  Created by Rita Casiello on 01/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import XCTest
@testable import Westeros

class HouseTests: XCTestCase {
    
    var starkSigil: Sigil!
    var lannisterSigil: Sigil!
    
    var starkURL: URL!
    var lannisterURL: URL!
    
    var starkHouse: House!
    var lannisterHouse: House!
    
    var ned: Person!
    var robb: Person!
    var tyrion: Person!
    var arya: Person!
    
    override func setUp() {
        starkSigil = Sigil(description: "Lobo Huargo", image: UIImage())
        lannisterSigil = Sigil(description: "León Rampante", image: UIImage())
        
        starkURL = URL(string: "https://awoiaf.westeros.org/index.php/House_Stark")!
        lannisterURL = URL(string: "https://awoiaf.westeros.org/index.php/House_Lannister")!
        
        starkHouse = House(name: "Stark", sigil:starkSigil, words:"Winter is coming", url: starkURL)
        lannisterHouse = House(name: "Lannister", sigil:lannisterSigil, words:"Hear my roar", url: lannisterURL)
        
        ned = Person(name: "Eddard", photo: UIImage(named: "robb")!, alias: "Ned", house: starkHouse)
        robb = Person (name: "Robb", photo: UIImage(named: "robb")!, alias: "joven Lobo", house: starkHouse)
        arya = Person (name: "Arya", house: starkHouse, photo: UIImage(named: "arya")!)
        tyrion = Person(name: "Tyrion", photo: UIImage(named: "tyrion")!, alias: "El enano", house: lannisterHouse)
       
    }

    override func tearDown() {
     
    }
    func testHouseExistence(){
        XCTAssertNotNil(starkHouse)
    }
    func testSigilExistence(){
        XCTAssertNotNil(starkSigil)
        
    }
    func testHouseAddPerson(){
        XCTAssertEqual(starkHouse.count, 0)
        
        starkHouse.addPerson(person: robb)
        XCTAssertEqual(starkHouse.count, 1)
        
        starkHouse.addPerson(person: arya)
        XCTAssertEqual(starkHouse.count, 2)
        
        starkHouse.addPerson(person: arya)
        XCTAssertEqual(starkHouse.count, 2)
        
        starkHouse.addPerson(person: tyrion)
        XCTAssertEqual(starkHouse.count, 2)
    }
    func testHouseAddPersonsAtOnce(){
        starkHouse.addPerson(persons: robb, arya, tyrion)
        XCTAssertEqual(starkHouse.count, 2)

    }
    func testHouseEquality(){
        //identidad
        XCTAssertEqual(starkHouse, starkHouse)
        //igualdad
        let jinxedHouse = House(name: "Stark", sigil:starkSigil, words:"Winter is coming", url: starkURL)
        XCTAssertEqual(jinxedHouse, starkHouse)
        //desigualdad
        XCTAssertNotEqual(starkHouse, lannisterHouse)
    }
    
    func testHouseComparison(){
        XCTAssertLessThan(lannisterHouse, starkHouse)

    }
    
    func testHouseSortedMembersReturnsaSortedListOfMembers(){
        XCTAssertEqual(starkHouse.sortedMembers, starkHouse.sortedMembers.sorted())
        
    }
}
