//
//  RepositoryTests.swift
//  WesterosTests
//
//  Created by Rita Casiello on 06/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import XCTest
@testable import Westeros

class RepositoryTests: XCTestCase {
    
    var houses = Repository.local.houses
    var seasons = Repository.local.seasons
    

    override func setUp() {
        
    }

    override func tearDown() {
       
    }
    func testLocalRepositoryExistence(){
        XCTAssertNotNil(Repository.local)
        
    }
    
    func testLocalFactoryHasHouses(){
        XCTAssertNotNil(houses)
        
    }
    //comprueba que la cantidad de casas coincida con los elementos que tiene el array
    func testLocalFactoryHasTheCorrectHouseCount(){
        XCTAssertEqual(houses.count, 3)
        
    }
    //comprueba que el array esté ordenado
    func testLocalRepositoryReturnSortedArrayOfHouses(){
        XCTAssertEqual(houses, houses.sorted())
    }
    
    func testLocalRepositoryReturnsHouseByNameCaseInsensitively(){
        let stark = Repository.local.house (named:"Stark")
        XCTAssertNotNil(stark)
        
        let lannister = Repository.local.house(named: "LAnniSTer")
        XCTAssertNotNil(lannister)
        
        let keepcoding = Repository.local.house(named: "Keepcoding")
        XCTAssertNil(keepcoding)
        
    }
    
    func testLocalRepositoryHouseFiltering(){
        let filteredHouseList = Repository.local.houses(filteredBy: { house in
            house.count == 1  //true or false
        })
        XCTAssertEqual(filteredHouseList.count, 1)
        
    let wordsFilter = Repository.local.houses (filteredBy: { house in
        house.words == "Winter is coming"
    })
    //cuantas casas tienen el siguiente lema? solo una
    XCTAssertEqual(wordsFilter.count, 1)
    
    }
    
    func testLocalFactoryHasTheCorrectSeasonCount(){
        XCTAssertEqual(seasons.count, 8)
        
    }
    
    func testLocalRepositoryReturnSortedArrayOfSeasons(){
       XCTAssertEqual(seasons, seasons.sorted())
        
    }
    
    func testLocalRepositoryReturnsSeasonsByNameCaseInsensitive(){
    let season1 = Repository.local.season (named: "Season 1")
    XCTAssertNotNil(season1)
       
    let season2 = Repository.local.season(named: "sEaSON 2")
        XCTAssertNotNil(season2)
        
    let starwars = Repository.local.season(named: "Star Wars")
        XCTAssertNil(starwars)
    }
    
    func testLocalRepositorySeasonFiltering(){
        let nameFiltered = Repository.local.seasons (filteredBy: { season in
            season.name == "Season 1"
        })
        XCTAssertEqual(nameFiltered.count, 1)

    }
}
