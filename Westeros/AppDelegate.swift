//
//  AppDelegate.swift
//  Westeros
//
//  Created by Rita Casiello on 01/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    var houseDetailNavigation: UINavigationController!
    var seasonDetailNavigation: UINavigationController!
    var houseListNavigation: UINavigationController!
    var seasonListNavigation: UINavigationController!
    var houseDetailViewController: HouseDetailViewController!
    var seasonDetailViewController: SeasonDetailViewController!
    var splitView: UISplitViewController!
    var tabBar: UITabBarController!
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        //creamos el modelo
        let houses = Repository.local.houses
        let seasons = Repository.local.seasons
        
        //creamos los controladores
        let houseListViewController = HouseListViewController(model:houses)
        let lastHouseSelected = houseListViewController.lastSelectedHouse()
         houseDetailViewController = HouseDetailViewController(model: lastHouseSelected)
        
        let seasonListViewController = SeasonListViewController(model:seasons)
        seasonDetailViewController = SeasonDetailViewController(model: seasons.first!)
        
        
        //asignamos el delegado
        houseListViewController.houseListdelegate = houseDetailViewController
        seasonListViewController.seasonListDelegate = seasonDetailViewController
        
        
        //envolvemos los controladores en navigations
        houseListNavigation = houseListViewController.wrappedInNavigation()
        seasonListNavigation = seasonListViewController.wrappedInNavigation()
        houseDetailNavigation = houseDetailViewController.wrappedInNavigation()
        seasonDetailNavigation = seasonDetailViewController.wrappedInNavigation()
        
        
        
        //MARK: - TabBar
        
        tabBar = UITabBarController()
        tabBar.viewControllers = [houseListNavigation, seasonListNavigation]
        tabBar.delegate = self
   
        houseListNavigation.tabBarItem = UITabBarItem(title: "Houses", image: nil, tag: 0)
        seasonListNavigation.tabBarItem = UITabBarItem(title: "Seasons", image: nil, tag: 1)
        
    
        //MARK: - SplitView
        splitView = UISplitViewController()
        
        splitView.viewControllers = [
            tabBar,    //master
            houseDetailNavigation, seasonDetailNavigation //detail
            ]
        
    //orientación landscape de inicio
            splitView?.preferredDisplayMode = .primaryOverlay
    
        
        //Asignamos el rootViewController
        window?.rootViewController = splitView
        
        return true
        
    }

}
extension AppDelegate: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let tabBarIndex = tabBar.selectedIndex
        
        if tabBarIndex == 0 {
            splitView.showDetailViewController(houseDetailNavigation, sender: nil)
            
        }else if tabBarIndex == 1 {
            splitView.showDetailViewController(seasonDetailNavigation, sender: nil)
        }
    }
    
}
