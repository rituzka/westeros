//
//  Person.swift
//  Westeros
//
//  Created by Rita Casiello on 02/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

final class Person {
    
    let name: String
    let photo: UIImage
    let house:House
    
    private let _alias : String?
    
    
    var alias: String {
    return _alias ?? ""
    }
    
    var fullname: String {
        return "\(name) \(house.name)"
        
    }
    
    
    init(name: String, photo: UIImage, alias: String?, house: House){
        self.name = name
        _alias = alias
        self.house = house
        self.photo = photo
        
    }
    convenience init(name: String, house: House, photo:UIImage){
        self.init(name: name,photo: photo, alias: nil,  house: house)
        
    }
}

extension Person {
    var proxyForEquality: String{
        return "\(name) \(alias) \(house.name)"
        
    }
    var proxyForComparison: String {
        return fullname.uppercased()
    }
    
}
extension Person: Equatable{
    static func == (lhs: Person, rhs: Person) -> Bool {
        return lhs.proxyForEquality == rhs.proxyForEquality
    }
    
}
extension Person: Hashable {
    var hashValue: Int {
        return proxyForEquality.hashValue
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine (name)
        hasher.combine (alias)
        hasher.combine (house.name)
        
            }
       }
extension Person: Comparable {
    static func < (lhs: Person, rhs: Person) -> Bool {
        return lhs.proxyForComparison < rhs.proxyForComparison
    }    
}

