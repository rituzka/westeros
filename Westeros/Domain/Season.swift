//
//  Season.swift
//  Westeros
//
//  Created by Rita Casiello on 10/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

typealias Episodes = Set<Episode>

class Season {
    
//properties
    let name: String
    let launchDate: Date
    let photoSeason: UIImage
    private var seasonEpisodes: Episodes

//init
    init(name: String, launchDate: Date, photoSeason: UIImage){
        self.name = name
        self.launchDate = launchDate
        self.photoSeason = photoSeason
        self.seasonEpisodes = []
    }
}
extension Season {
    var proxyForEquality: String {
        return name.uppercased()
    }
    var proxyForComparison: Date {
        return launchDate
    }
    
}

extension Season: Comparable {
    static func < (lhs: Season, rhs: Season) -> Bool {
        return lhs.proxyForComparison < lhs.proxyForComparison
    }
}
extension Season: Equatable {
    static func == (lhs: Season, rhs: Season) -> Bool {
        return lhs.proxyForEquality == rhs.proxyForEquality
    }
}
extension Season: Hashable {
    var hashValue: Int {
       return proxyForEquality.hashValue
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine (name)
    }
}
extension Season {
    
    var count: Int {
        return seasonEpisodes.count
    }
    
    func add (episode: Episode){
        if self == episode.season {
           seasonEpisodes.insert(episode)
        }
    }

    func add(episodes: Episode...){
        for episode in episodes {
        add (episode: episode)
        }
    }
    
    var sortedSeasonEpisodes:[Episode]{
        return seasonEpisodes.sorted()}
}
