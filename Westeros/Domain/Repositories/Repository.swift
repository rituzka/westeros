//
//  Repository.swift
//  Westeros
//
//  Created by Rita Casiello on 06/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit


final class Repository {
    
    static let local: HouseFactory = LocalFactory()
    
}

protocol HouseFactory {
    var houses: [House] { get }
    var seasons: [Season] { get }
    
    //creamos una función que acepte un string y me devuelva una casa
    func house (named: String) -> House?
    //creamos una función que acepte un string y me devuelva una temporada
    func season (named: String) -> Season?

    
    typealias HouseFilter = (House) -> Bool
    func houses (filteredBy: HouseFilter) -> [House]
    
    typealias SeasonFilter = (Season) -> Bool
    func seasons (filteredBy: SeasonFilter) -> [Season]
}

final class LocalFactory: HouseFactory {
  
    var houses: [House] {
        //creamos las casas
        
        //Escudos
        let starkSigil = Sigil(description: "Lobo Huargo", image: UIImage(named: "stark")! )
        let lannisterSigil = Sigil(description: "León Rampante", image: UIImage(named: "lannister")!)
        let targaryenSigil = Sigil(description: "Dragón tricéfalo", image: UIImage(named: "targaryen")!)
        
        //URL's
        let starkURL = URL(string: "https://awoiaf.westeros.org/index.php/House_Stark")!
        let lannisterURL = URL(string: "https://awoiaf.westeros.org/index.php/House_Lannister")!
        let targaryenURL = URL(string: "https://awoiaf.westeros.org/index.php/House_Targaryen")!
        
        //casas
        let lannisterHouse = House(name: "Lannister", sigil: lannisterSigil, words: "Hear my roar", url: lannisterURL)
        let starkHouse = House(name: "Stark", sigil: starkSigil, words: "Winter is coming", url: starkURL)
        let targaryenHouse = House(name: "Targaryen", sigil: targaryenSigil, words: "Blood and Fire", url: targaryenURL)
        
        //personajes
        let robb = Person (name: "Robb", photo: UIImage(named: "robb")!, alias: "joven Lobo", house: starkHouse)
        let arya = Person (name: "Arya", house: starkHouse, photo: UIImage(named: "arya")!)
        let tyrion = Person(name: "Tyrion", photo: UIImage(named: "tyrion")!, alias: "El enano", house: lannisterHouse)
        let jaime = Person(name: "Jaime",photo:UIImage(named: "jaime")!, alias: "El Matarreyes", house: lannisterHouse)
        let cercei = Person (name: "Cercei", house: lannisterHouse,photo:UIImage(named: "cercei")!)
        let dani = Person (name: "Daenerys", photo: UIImage(named: "dani")!,alias: "La Madre de Dragones", house: targaryenHouse)
        
        //añadimos los personajes a las casas
        starkHouse.addPerson(persons: robb, arya)
        lannisterHouse.addPerson(persons: tyrion, jaime, cercei)
        targaryenHouse.addPerson(person: dani)
   
        return[starkHouse, lannisterHouse, targaryenHouse].sorted()
        
        }
    
    
    func house(named name: String) -> House? {
        return houses.filter{ $0.name.uppercased() == name.uppercased()}.first
    }
    
    func houses(filteredBy theFilter: (House) -> Bool) -> [House] {
        return houses.filter(theFilter)
    }
    
    var seasons: [Season] {
 
       //temporadas
        let season1 = Season(
            name: "Season 1",
            launchDate: Date.parse("17/04/2011"),
            photoSeason: UIImage(named: "season1")!
        )
        let season2 = Season(
            name: "Season 2",
            launchDate: Date.parse("01/04/2012"),
            photoSeason: UIImage (named: "season2")!
        )
        let season3 = Season(
            name: "Season 3",
            launchDate: Date.parse("31/03/2013"),
            photoSeason: UIImage (named: "season3")!
        )
        let season4 = Season(
            name: "Season 4",
            launchDate: Date.parse("06/04/2014"),
            photoSeason: UIImage(named: "season4")!
        )
        let season5 = Season(
            name: "Season 5",
            launchDate: Date.parse("12/04/2015"),
            photoSeason: UIImage (named: "season5")!
        )
        let season6 = Season(
            name: "Season 6",
            launchDate: Date.parse("24/04/2016"),
            photoSeason: UIImage (named: "season6")!
        )
        let season7 = Season(
            name: "Season 7",
            launchDate: Date.parse("16/07/2017"),
            photoSeason: UIImage (named: "season7")!
        )
        let season8 = Season(
            name: "Season 8",
            launchDate: Date.parse("14/04/2019"),
            photoSeason: UIImage (named: "season8")!
        )
        
        //episodios
        let ep1s1 = Episode(
            title: "Winter is coming",
            dateIssue: Date.parse("17/04/2011"),
            description: "'Winter Is Coming' is the series premiere episode of the HBO medieval fantasy television series Game of Thrones. The first episode of the first season, it was written by the show creators David Benioff and D. B. Weiss, in a faithful adaptation of the first chapters of George R. R. Martin's book A Game of Thrones. The episode was directed by Tim Van Patten, redoing the work done by director Tom McCarthy in an unaired pilot.",
            season: season1
        )
        let ep2s1 = Episode(
            title: "The Kingsroar",
            dateIssue: Date.parse("24/04/2011"),
            description: "'The Kingsroad' is the second episode of the first season of the HBO medieval fantasy television series Game of Thrones, first aired on April 24, 2011. It was written by the show creators David Benioff and D. B. Weiss, and directed by Tim Van Patten.",
            season: season1
        )
        let ep1s2 = Episode(
            title: "The North remembers",
            dateIssue: Date.parse("01/04/2012"),
            description: "'The North Remembers' is the second season premiere episode of HBO's fantasy television series Game of Thrones. First aired on April 1, 2012, it was written by the show creators and executive producers David Benioff and D. B. Weiss, and directed by returning director Alan Taylor.",
            season: season2
        )
        let ep2s2 = Episode(
            title: "The Night Lands",
            dateIssue: Date.parse("08/04/2012"),
            description: "'The Night Lands' is the second episode of the second season of HBO's medieval fantasy television series Game of Thrones. The episode is written by the showrunners David Benioff and D. B. Weiss, and directed by Alan Taylor. It was first released on April 2, 2012 via the online service HBO GO in some European countries, including the Netherlands, Poland and Slovenia. Cable television first broadcast it on April 8, 2012.",
            season: season2
        )
        let ep1s3 = Episode(
            title: "Valar Dohaeris",
            dateIssue: Date.parse("31/03/2013"),
            description: "'Valar Dohaeris' is the third season premiere episode of the HBO fantasy television series Game of Thrones. Written by executive producers David Benioff and D. B. Weiss, and directed by Daniel Minahan, it aired on March 31, 2013.",
            season: season3
        )
        let ep2s3 = Episode(
            title: "Dark Wings, Dark Words",
            dateIssue: Date.parse("07/04/2013"),
            description: "'Dark Wings, Dark Words' is the second episode of the third season of HBO's fantasy television series Game of Thrones, and the 22nd episode of the series. Written by Vanessa Taylor, and directed by Daniel Minahan, it aired on April 7, 2013.",
            season: season3
        )
        let ep1s4 = Episode(
            title: "Two Swords",
            dateIssue: Date.parse("06/04/2014"),
            description: "'Two Swords' is the fourth season premiere episode of HBO's fantasy television series Game of Thrones, and the 31st overall. The episode was written by series co-creators and showrunners David Benioff and D. B. Weiss, and directed by Weiss. It premiered on April 6, 2014.",
            season: season4
        )
        let ep2s4 = Episode(
            title: "The Lion and the Rose",
            dateIssue: Date.parse("13/04/2014"),
            description: "'The Lion and the Rose' is the second episode of the fourth season of HBO's fantasy television series Game of Thrones, and the 32nd overall. The episode was written by George R. R. Martin, the author of the A Song of Ice and Fire novels of which the series is an adaptation, and directed by Alex Graves. It aired on April 13, 2014.",
            season: season4
        )
        let ep1s5 = Episode(
            title: "The Wars to Come",
            dateIssue: Date.parse("12/04/2015"),
            description: "'The Wars to Come' is the fifth season premiere episode of HBO's fantasy television series Game of Thrones, and the 41st overall. The first episode of the fifth season, it aired on April 12, 2015.",
            season: season5
        )
        let ep2s5 = Episode(
            title: "The House of Black and White",
            dateIssue: Date.parse("19/04/2015"),
            description: "'The House of Black and White' is the second episode of the fifth season of HBO's fantasy television series Game of Thrones, and the 42nd overall. The episode was written by series co-creators David Benioff and D. B. Weiss, and directed by Michael Slovis. It aired on April 19, 2015. Prior to airing, this episode along with the other first four episodes of the season were leaked online.",
            season: season5
        )
        let ep1s6 = Episode(
            title: "The Red Woman",
            dateIssue: Date.parse("24/04/2016"),
            description: "'The Red Woman' is the sixth season premiere episode of HBO's fantasy television series Game of Thrones, and the 51st overall. The episode was written by series creators David Benioff and D. B. Weiss, and directed by Jeremy Podeswa.",
            season: season6
        )
        let ep2s6 = Episode(
            title: "Home",
            dateIssue: Date.parse("01/05/2016"),
            description: "'Home' is the second episode of the sixth season of HBO's fantasy television series Game of Thrones, and the 52nd overall. The episode was written by Dave Hill and directed by Jeremy Podeswa.",
            season: season6
        )
        let ep1s7 = Episode(
            title: "Dragonstone",
            dateIssue: Date.parse("16/07/2017"),
            description: "'Dragonstone' is the seventh season premiere episode of HBO's fantasy television series Game of Thrones, and the 61st overall. It was written by series co-creators David Benioff and D. B. Weiss, and directed by Jeremy Podeswa.",
            season: season7
        )
        let ep2s7 = Episode(
            title: "Stormborn",
            dateIssue: Date.parse("23/07/2017"),
            description: "'Stormborn' is the second episode of the seventh season of HBO's fantasy television series Game of Thrones, and the 62nd overall. The episode was written by Bryan Cogman, and directed by Mark Mylod. The title of the episode refers to both Daenerys Targaryen, who was born during a terrible storm, and Euron Greyjoy, who declares himself to be 'the storm'.",
            season: season7
        )
        let ep1s8 = Episode(
            title: "Winterfell",
            dateIssue:Date.parse("14/04/2019"),
            description: "'Winterfell' is the eighth season premiere episode of HBO's fantasy television series Game of Thrones, and the 68th overall. It was written by Dave Hill and directed by David Nutter. It aired on April 14, 2019.",
            season: season8
        )
        let ep2s8 = Episode(
            title: "A Night of the Seven Kingdoms",
            dateIssue: Date.parse("21/04/2019"),
            description: "'A Knight of the Seven Kingdoms' is the second episode of the eighth season of HBO's fantasy television series Game of Thrones, and the 69th overall. It was written by Bryan Cogman, and directed by David Nutter. It aired on April 21, 2019.",
            season: season8
        )
       
        //añadimos los episodios a las temporadas
        season1.add(episodes: ep1s1, ep2s1)
        season2.add(episodes: ep1s2, ep2s2)
        season3.add(episodes: ep1s3, ep2s3)
        season4.add(episodes: ep1s4, ep2s4)
        season5.add(episodes: ep1s5, ep2s5)
        season6.add(episodes: ep1s6, ep2s6)
        season7.add(episodes: ep1s7, ep2s7)
        season8.add(episodes: ep1s8, ep2s8)
        
    return [season1, season2, season3, season4, season5, season6, season7,season8].sorted()
        
    }
    func season(named name: String) -> Season? {
        return seasons.filter{ $0.name.uppercased() == name.uppercased()}.first
    }
    
    func seasons(filteredBy theFilter: (Season) -> Bool) -> [Season] {
        return seasons.filter(theFilter)
    }
        
  
}
