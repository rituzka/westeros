//
//  House.swift
//  Westeros
//
//  Created by Rita Casiello on 01/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import Foundation

typealias Words = String
typealias Members = Set <Person>

final class House {

let name:String
let sigil: Sigil
let words: Words
let wikiURL : URL
    private var _members: Members

    init(name:String, sigil: Sigil, words: Words, url: URL){
    self.name = name
    self.sigil = sigil
    self.words = words
    self.wikiURL = url
    self._members = []
    
    }    
}

extension House {
    var proxyForEquality: String {
        return "\(name) \(words)"
    }
    
    var proxyForComparison: String {
        return name.uppercased()
        
        //si lo quisiera comparar por tamaño de escudo => sería de tipo SigilSize
        //o por número de miembros sería tipo Int
        //como los quiero ordenar alfabeticamente lo hago de tipo String y devuelvo el nombre de house para poder comparar las dos casas.
        
    }
}
extension House: Comparable {
    static func < (lhs: House, rhs: House) -> Bool {
        return lhs.proxyForComparison < rhs.proxyForComparison
    }

}
extension House: Equatable {
    static func == (lhs: House, rhs: House) -> Bool {
        return lhs.proxyForEquality == rhs.proxyForEquality
    }
}
extension House {
    var count: Int {
        return _members.count
    }
   
    func addPerson (person: Person){
        if self == person.house {
        _members.insert(person)
        }
    }
     //funciones variádicas, se declaran con tres puntos
    func addPerson (persons: Person...){
       // persons.forEach { addPerson(person: $0)}
        for person in persons {
            addPerson(person: person)
        }
    }
    var sortedMembers:[Person]{
        return _members.sorted()
    }
    
}
