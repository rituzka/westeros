//
//  Sigil.swift
//  Westeros
//
//  Created by Rita Casiello on 01/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit


final class Sigil {
    
    let description: String
    let image: UIImage
    
    init(description: String, image: UIImage){
        self.description = description
        self.image = image
        
    }
    
}
