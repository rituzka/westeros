//
//  Episode.swift
//  Westeros
//
//  Created by Rita Casiello on 10/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import Foundation



final class Episode {
    
    let title: String
    let dateIssue: Date
    let description: String
     weak var season: Season?
    
    
    
    init(title: String, dateIssue: Date, description: String, season: Season){
        self.title = title
        self.dateIssue = dateIssue
        self.description = description
        self.season = season

    }
}

extension Episode {
    var proxyforEquality: String {
        return "\(title) \(dateIssue)"
    }
    
    var proxyforComparison: String {
        return title.uppercased()
        
    }
}

extension Episode: Equatable {
    static func == (lhs: Episode, rhs: Episode) -> Bool {
        return lhs.proxyforEquality == rhs.proxyforEquality
    }
}

extension Episode: Comparable {
    static func < (lhs: Episode, rhs: Episode) -> Bool {
        return lhs.proxyforComparison < rhs.proxyforComparison
    }
}

extension Episode: Hashable {
    var hashValue: Int {
        return proxyforEquality.hashValue
    }
        func hash(into hasher: inout Hasher){
            hasher.combine (title)
            hasher.combine (dateIssue)
             }
    
}
