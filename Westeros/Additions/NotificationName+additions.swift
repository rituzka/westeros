//
//  NotificationName+additions.swift
//  Westeros
//
//  Created by Rita Casiello on 09/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let houseDidNotificationName = Notification.Name ("HouseDidNotificationName")
    static let seasonDidNotificationName = Notification.Name ("SeasonDidNotificationName")
}
