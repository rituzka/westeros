//
//  Data.swift
//  Westeros
//
//  Created by Rita Casiello on 11/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import Foundation

extension Date {
    
    static func parse(_ string: String, format: String = "dd/MM/yyyy") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        let date = dateFormatter.date(from: string)!
        return date
    }
    
    static func datetoString (dateConvert: Date) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = .short
        
        let dateToString = dateformatter.string(from: dateConvert)
        return dateToString
    }
}
