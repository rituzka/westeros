//
//  UIViewController+navigation.swift
//  Westeros
//
//  Created by Rita Casiello on 06/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func wrappedInNavigation() -> UINavigationController {
        //OJO aqui vamos a crear una nueva instancia de UINavigationController. CREA UN CONTROLADOR NUEVO!
        return UINavigationController(rootViewController: self)
    }
}
