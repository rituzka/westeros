//
//  MembersDetailViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 18/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

class MembersDetailViewController: UIViewController {
    
    //MARK: - outlets
    @IBOutlet weak var memberNameLabel: UILabel!    
    @IBOutlet weak var photoMemberLabel: UIImageView!
    @IBOutlet weak var aliasMemberLabel: UILabel?
   
    //MARK: - Properties
    var model: Person
    
    init (model: Person){
        self.model = model
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        syncModelWithView()
        
    }
    func syncModelWithView() {
        memberNameLabel.text = model.fullname
        photoMemberLabel.image = model.photo
        aliasMemberLabel?.text = model.alias
    }
}
extension MembersDetailViewController: MemberListViewControllerDelegate {
    func memberListViewController(_viewController: MembersListViewController, didSelectMember member: Person) {
        model = member
        syncModelWithView()
        
    }

}
