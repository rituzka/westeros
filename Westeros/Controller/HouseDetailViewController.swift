//
//  HouseViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 04/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

final class HouseDetailViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var houseNameLabel: UILabel!
    @IBOutlet private weak var houseImage: UIImageView!
    @IBOutlet private weak var houseWordsLabel: UILabel!
    
    //MARK: - Properties
    private var model: House
    
    //MARK: - Inicializer
    init (model:House){
        self.model = model
        super.init(nibName: nil, bundle: nil)
        title = model.name
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        syncModelWithView()
        setupUI()
        
    }
}

extension HouseDetailViewController {
    private func syncModelWithView(){
        houseNameLabel.text = "House \(model.name)"
        houseImage.image = model.sigil.image
        houseWordsLabel.text = model.words
        
    }
}
extension HouseDetailViewController{
    private func setupUI(){
     //creo botón wiki
        let wikiButton = UIBarButtonItem (
            title: "Wiki",
            style: UIBarButtonItem.Style.plain,
            target: self, //donde está definido el metodo del action
            action: #selector(displayWiki) //metodo que se ejecutará al iniciarse el evento
        )
        
        //creo botón MemberList
        let membersButton = UIBarButtonItem (
            title: "Members",
            style: UIBarButtonItem.Style.plain,
            target: self, //donde está definido el metodo del action
            action: #selector(displayMembers) //metodo que se ejecutará al iniciarse el evento
        )
       
        //añado a navigationBar
        navigationItem.setRightBarButtonItems([membersButton,wikiButton], animated: true)
        //son propiedades del botón de navigation
    
    }
    @objc private func displayWiki(){
        //creamos el wiki VC
        let wikiVC = WikiViewController(model: model)
        //lanzamos el controlador mediante un push
        navigationController?.pushViewController(wikiVC, animated: true)
        
    }
    
    @objc private func displayMembers(){
        //creamos el members VC
        let membersVC = MembersListViewController(model: model.sortedMembers)
        //lanzamos el controlador mediante un push
        navigationController?.pushViewController(membersVC, animated: true)
    }
}
//MARK: - Delegate
extension HouseDetailViewController: HouseListViewControllerDelegate {
    func houseListViewController(_ viewController: HouseListViewController, didSelectHouse house: House) {
        model = house
        syncModelWithView()
    }

}
