//
//  EpisodeDetailViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 18/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

class EpisodeDetailViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var seasonNameLabel: UILabel!
    @IBOutlet weak var episodeNameLabel: UILabel!
    @IBOutlet weak var descriptionEpisodeLabel: UILabel!
    
    
    //MARK: - Properties
    let model: Episode
    
    init (model: Episode){
        self.model = model
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Cycle life app
    override func viewDidLoad() {
        super.viewDidLoad()
        syncModelWithView()

    }

    func syncModelWithView(){
        seasonNameLabel.text = model.season?.name
        episodeNameLabel.text = model.title
        descriptionEpisodeLabel.text = model.description
        
    }
}

