//
//  SeasonDetailViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 13/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

class SeasonDetailViewController: UIViewController {

    @IBOutlet weak var seasonNameLabel: UILabel!
    @IBOutlet weak var seasonPhotoImage: UIImageView!
    @IBOutlet weak var seasonLaunchDateLabel: UILabel!
    
    var model: Season
    
    init(model: Season){
        self.model = model
        super.init(nibName: nil, bundle: Bundle (for: type(of: self)))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        syncModelWithView()
        setUpUI()
    }
    
    func syncModelWithView(){
        seasonNameLabel.text = model.name
        seasonPhotoImage.image = model.photoSeason
        seasonLaunchDateLabel.text = Date.datetoString(dateConvert: model.launchDate)
    }
}

extension SeasonDetailViewController: SeasonListViewControllerDelegate {
    func seasonListViewController(_viewController: SeasonListViewController, didSelectedSeason season: Season) {
        model = season
        syncModelWithView()
    }
}

extension SeasonDetailViewController {
    
    func setUpUI(){
        let buttonEpisodes = UIBarButtonItem(
            title: "Episodes",
            style: .plain,
            target: self,
            action: #selector(displayEpisodes)
        )
        
        navigationItem.setRightBarButton(buttonEpisodes, animated: true)
    }
    
    @objc func displayEpisodes(){
        let episodesVC = EpisodeListViewController(model: model.sortedSeasonEpisodes)
        navigationController?.pushViewController(episodesVC, animated: true)
    }
}
