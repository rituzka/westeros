//
//  MembersListViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 08/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

protocol MemberListViewControllerDelegate: class {
    func memberListViewController(_viewController:MembersListViewController, didSelectMember member: Person)
}

class MembersListViewController: UIViewController{
    
    //MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    
    //MARK: - Properties
    var model: [Person]
    weak var memberDelegate: MemberListViewControllerDelegate?
    
    //MARK: - Init
    init(model: [Person]){
        self.model = model
        super.init(nibName: nil, bundle: nil)
        title = "Members"
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Cycle life
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        //suscribir a las notificaciones
       subscribeToNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    //dessuscribir a las notificaciones
        unsubscribeNotifications()
    }
}
//MARK: - TableView DataSource
extension MembersListViewController: UITableViewDataSource {
 
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1 //no tenemos secciones
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection
        section: Int) -> Int {
        return model.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "PersonCell"
        
        //Descubrir cual son los miembros que se va a mostrar
        let members = model[indexPath.row]
        
        //Crear una celda
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
            ??  UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
        
        //sincronizar modelo(persons) - vista(celda)
        cell.textLabel?.text = members.fullname
        cell.detailTextLabel?.text = members.alias
       
        //devolver celda
        return cell
        
    }
}

extension MembersListViewController: UITableViewDelegate{

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //averiguar qué miembro fue seleccionado
        let member = model[indexPath.row]
        
        //avisar al delegado
        memberDelegate?.memberListViewController(_viewController: self, didSelectMember: member)
        
        // push a Member Detail
        let memberDetailViewController = MembersDetailViewController(model: member)
        navigationController?.pushViewController(memberDetailViewController, animated: true)
    }
    
    @objc func houseDidChange(notification: Notification){
     
        //userInfo
         guard let dictionary = notification.userInfo else {
            return
        }
         //averiguamos la casa
        guard let house =  dictionary[HouseListViewController.Constants.houseKey] as? House else{
            return
       }
        
        //actualizar el modelo
        model = (house.sortedMembers)
        
        //actualizar datos y vista de la table view
        tableView.reloadData()
    }
}

extension MembersListViewController {
    func subscribeToNotifications(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector (houseDidChange), name: .houseDidNotificationName, object: nil)
    }
    
    func unsubscribeNotifications(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
    }
}
    
        
        
        
        
        
        
        
        
        
        
    
    





