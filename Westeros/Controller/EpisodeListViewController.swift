//
//  EpisodeListViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 13/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

    
class EpisodeListViewController: UITableViewController {
    
    //MARK: - properties
    var model: [Episode]
    
    
    //MARK: - init
    init (model: [Episode]){
        self.model = model
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //MARK: - Cycle life
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //suscribir a notificaciones
        subscribeToNotifications()
    }
    override func viewWillDisappear(_ animated: Bool) {
        //dessuscribir de las notificaciones
        unsuscribeToNotifications()
    }
   
    //MARK: - Table Data Source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }

    //MARK: - table delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID = "episodeCell"
        //descubrir episodios que se van a mostrar en las celdas
        let episodes = model[indexPath.row]
        
        //crear la celda
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) ?? UITableViewCell(style: .subtitle, reuseIdentifier: cellID)
        
        //sincronizar modelo y vista
        cell.textLabel?.text = episodes.title
        cell.detailTextLabel?.text = Date.datetoString(dateConvert: episodes.dateIssue)
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    @objc func seasonDidChange(notification: Notification){
        //userInfo
        guard let dictionary = notification.userInfo else {
            return
        }
        //averiguamos la temporada
        guard let season =  dictionary[SeasonListViewController.Constants.seasonKey] as? Season else{
            return
        }
        
        //actualizar el modelo
        model = (season.sortedSeasonEpisodes)
        
        //actualizar datos y vista de la table view
        tableView.reloadData()
        }
    //MARK: - tableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let episode = model[indexPath.row]
        let episodeDetail = EpisodeDetailViewController(model: episode)
        navigationController?.pushViewController(episodeDetail, animated: true)
        }
        
    }

extension EpisodeListViewController {
    func subscribeToNotifications(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(seasonDidChange), name: .seasonDidNotificationName, object: nil)
    }
    
    func unsuscribeToNotifications(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)    }
    
    
    
}
