//
//  HouseListViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 07/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit



protocol HouseListViewControllerDelegate: class {
   func houseListViewController(_ viewController: HouseListViewController, didSelectHouse house: House)
  
}

class HouseListViewController: UITableViewController {
    
    enum Constants {
        
       static let houseKey = "HouseKey"
       static let lastHouseKey = "LastHouseKey"
        
    }
        
        
    //MARK: - Properties
    let model: [House]
    weak var houseListdelegate: HouseListViewControllerDelegate?
    
    
    //MARK: - Init
     init(model: [House]) {
        self.model = model
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        title = "Westeros"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    //MARK: - TableView Data Source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 //no tenemos secciones
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection
        section: Int) -> Int {
        return model.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "HouseCell"
        
        //Descubrir cual es la casa que se va a mostrar
        let house = model[indexPath.row]
        
        //Crear una celda
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
         ??  UITableViewCell(style: .default, reuseIdentifier: cellId)
        
        //sincronizar modelo(house) - vista(celda)
        cell.textLabel?.text = house.name
        cell.imageView?.image = house.sigil.image
        
        //devolver celda
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Averiguar qué casa se ha pulsado
        let house = model[indexPath.row]
        
        //avisar al delegado que se conforme
        houseListdelegate?.houseListViewController(self, didSelectHouse: house)
        
        //mandamos la misma información a través de las notificaciones
        let notificationCenter = NotificationCenter.default //(singleton que devuelve una instancia)
        
        let dictionary = [Constants.houseKey: house]
        
        let notification = Notification(
            name: .houseDidNotificationName, //viene del archivo de additions
            object: self, //qué objeto envía las notificaciones
            userInfo: dictionary
        )
        
        notificationCenter.post(notification)
        
        //guardar ultima casa seleccionada. Usaremos el mecanismo de persistencia UserDefaults
        saveLastSelectedHouse(at: indexPath.row)
        
    }
}
extension HouseListViewController {
    
    //escribimos en userDefaults
    private func saveLastSelectedHouse(at index: Int) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(index, forKey: Constants.lastHouseKey)
        userDefaults.synchronize()
    }
    //leemos de userDefaults y hacemos la funcion pública porque la llamaremos desde fuera
    func lastSelectedHouse() -> House {
        let userDefaults = UserDefaults.standard
        let lastIndex = userDefaults.integer(forKey: Constants.lastHouseKey) // 0 is default en caso de nil
        
        //Devolvemos la casa con el índice
        return model[lastIndex]
    }
}
