//
//  WikiViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 07/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit
import WebKit

final class WikiViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet private weak var LoadingView: UIActivityIndicatorView!
    @IBOutlet private weak var webView: WKWebView!
  
    //MARK: - Properties
    //usaré más propiedades que solo la URL => mejor hacer la propiedad de tipo House
    private var model: House
    
    //MARK: - Init
   init(model: House) {
    self.model = model
    super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //se asigna el  delegado  que es esta misma clase para el objeto webView
        webView.navigationDelegate = self
        
        syncModelWithView()
        subscribeToNotifications()
        
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeNotifications()
    }
}
extension WikiViewController {
    private func syncModelWithView(){
        title = model.name
        
        //Mostramos y arrancamos el loadingView
        LoadingView.isHidden = false
        LoadingView.startAnimating()
        
        //cargamos la url
        let request = URLRequest (url: model.wikiURL)
        webView.load(request)
 
    }
}
extension WikiViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //detenemos el loadingView
        LoadingView.stopAnimating()
       
        //ocultamos el loadingVIew
        LoadingView.isHidden = true
        
    }
    func webView(
        _ webView: WKWebView,
        decidePolicyFor navigationAction: WKNavigationAction,
        decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //no permitir navegacion a links
        let type = navigationAction.navigationType//esta info está en el wkNavigationAction
        
        switch type {
        case .linkActivated:
            decisionHandler(.cancel)
        case .formSubmitted:
            decisionHandler(.cancel)
 
        @unknown default:
           decisionHandler (.allow)
        }
        //no permitir rellenar formularios
    }
}
extension WikiViewController {
    func subscribeToNotifications(){
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self, selector: #selector(houseDidChange), name: .houseDidNotificationName, object: nil)
        
    }
    
    func unsubscribeNotifications(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
    }
    
   @objc func houseDidChange(notification: Notification){
    
        //averiguar la casa (la casa viene en un dictionary del user info de las notifications)
    guard let dictionary = notification.userInfo else {
        return //esta es una manera de desempaquetar un opcional, si es nil se sale de la función
    }
    
    guard let house =  dictionary[HouseListViewController.Constants.houseKey] as? House else{
        return
    } // as? esto es un casting opcional, si lo ponemos con el as! es un casting por cojones
    
        //actualizar el modelo
        model = house
    
        //sincronizar modelo y vista
        syncModelWithView() 
    }
    
}
