//
//  SeasonListViewController.swift
//  Westeros
//
//  Created by Rita Casiello on 13/08/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

protocol SeasonListViewControllerDelegate: class {
    func seasonListViewController (_viewController: SeasonListViewController, didSelectedSeason season: Season)
}


class SeasonListViewController: UITableViewController {
    
    enum Constants {
        
        static let seasonKey = "SeasonKey"
        static let lastSeasonKey = "LastSeasonKey"
       
    }
    
    //MARK: - Properties
    let model: [Season]
     weak var seasonListDelegate: SeasonListViewControllerDelegate?
  
    
    //MARK: - Init
    init(model: [Season]){
        self.model = model
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       /* let cell = UINib(nibName: "MyCustomCell", bundle: Bundle (for: type(of: self)))
        tableView.register(cell, forCellReuseIdentifier: cellID)*/
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    //MARK: -Table view delegate
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID = "SeasonCell"
        
        //Descubrir la temporada que se va a mostrar
        let season = model[indexPath.row]
        
        //Crear una celda
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) ?? UITableViewCell(style: .default, reuseIdentifier: cellID)
   
        
        //sincronizar modelo(season) - vista(celda)
        cell.textLabel?.text = season.name
        cell.detailTextLabel?.text = Date.datetoString(dateConvert: season.launchDate)
        cell.imageView?.image = season.photoSeason
    
    return cell
        
    }
  /*  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? MyCustomCell else {
                return UITableViewCell()
            }
        let imageName = model[indexPath.row].photoSeason
        let name = model[indexPath.row].name
        
        cell.configure(imageName: imageName, name: name)
        return cell
        
        } */
        
 
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //averiguar qué temporada ha sido pulsada
        let season = model[indexPath.row]
        
        //avisar al delegado que se conforme
        seasonListDelegate?.seasonListViewController(_viewController: self, didSelectedSeason: season)
        
        //enviamos la misma información a través de las notificaciones
        let notificationCenter = NotificationCenter.default //(singleton que devuelve una instancia)
        
        let dictionary = [Constants.seasonKey: season]
        
        let notification = Notification(
            name: .seasonDidNotificationName, //viene del archivo de additions
            object: self,
            userInfo: dictionary
        )
        
        notificationCenter.post(notification)
        
        //guardar ultima temporada seleccionada. Usaremos el mecanismo de persistencia UserDefaults
        saveLastSelectedSeason(at: indexPath.row)
    }
}

extension SeasonListViewController {
    
    //escribimos en userDefaults
    private func saveLastSelectedSeason(at index: Int) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(index, forKey: Constants.lastSeasonKey)
        userDefaults.synchronize()
    }
    //leemos de userDefaults
    func lastSelectedSeason() -> Season {
        let userDefaults = UserDefaults.standard
        let lastIndex = userDefaults.integer(forKey: Constants.lastSeasonKey)
        
        //Devolvemos la temporada con el índice
        return model[lastIndex]
    }
}
